# Laravel SaaS Boilerplate custom setup utilities

These utilities will help in customizing some features of the Laravel SaaS Boilerplate by Samuel Štancl. The Boilerplate
is not public domain, you can visit https://tenancyforlaravel.com/docs/v3/introduction/ to obtain a legitimate copy of
it.

SaaS Boilerplate uses Nova as admin panel and needs some adjustement if you want to use a different one.

The [remove_nova.sh](boilerplate_custom_setup/remove_nova.sh) bash script wil try to do the annoying job of removing
Nova dependencies.

Remember: you use this software on your own responsibility. I recommend to run it on a scratch branch and verify
the correct results.