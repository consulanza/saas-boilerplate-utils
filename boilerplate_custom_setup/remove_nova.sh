#!/bin/bash
#
# remove NOVA dependencies from the boilerplate

function deleteMigrations() {
    files=( \
            "create_action_events_table.php" \
            "add_fields_to_action_events_table.php" \
            "change_actionable_id_to_string_in_action_events_table.php" \
            "create_nova_notifications_table.php" \
            "add_fields_to_nova_notifications_table.php" \
    )

    for name in "${files[@]}"
    do
      rm database/migrations/*_${name}
    done
}

function deleteTenantMigrations() {
    files=( \
        "tenant_create_action_events_table.php" \
        "tenant_add_fields_to_action_events_table.php" \
        "create_nova_notifications_table.php" \
        "add_fields_to_nova_notifications_table.php" \
    )
    for name in "${files[@]}"
    do
       rm database/migrations/tenant/*_${name}
    done
}

function removeBootstrapper() {
    rm  app/TenancyBootstrappers/NovaAuthGuardBootstrapper.php
    sed -i 's/   App\\TenancyBootstrappers\\NovaAuthGuardBootstrapper::class/   \/\/ App\\TenancyBootstrappers\\NovaAuthGuardBootstrapper::class/' \
        config/tenancy.php

    sed -i "s/{{ config('nova.path') }}/#/" \
        resources/views/layouts/tenant.blade.php
}

function removeServiceProvider() {
    rm app/Providers/NovaServiceProvider.php
    sed -i 's/   App\\Providers\\NovaServiceProvider::class/   \/\/  App\\Providers\\NovaServiceProvider::class/' config/app.php
    rm config/nova.php
    rm -rf app/Nova
}

function removeResources() {
    rm -rf lang/vendor/nova
    rm -rf public/vendor/nova
    rm -rf resources/views/vendor/nova
}

function clearComposer() {
    $COMPOSER remove --no-update laravel/nova tightenco/nova-stripe
    rm composer.lock
}

function addLaravelUi {
    $COMPOSER require --no-update laravel/ui
}

###########################################################
# main
#
# please update set_composer.sh to reflect your
# current composer executable
#
###########################################################

#
source $(dirname "$0")/set_composer.inc.sh
#
#
# remove unneeded packages
#
clearComposer
#
# nova service provider
#
removeServiceProvider
#
## migrations - main
#
deleteMigrations
#
## migration - tenant
#
deleteTenantMigrations
#
# tenancy bootstrapper
#
removeBootstrapper
#
# nova resources
#
removeResources
#
#
addLaravelUi
#
# final composer update
#
$COMPOSER update
