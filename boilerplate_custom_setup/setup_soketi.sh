#!/bin/bash
#
# setup soketi
#
source $(dirname "$0")/set_composer.inc.sh
#
$COMPOSER require --no-update pusher/pusher-php-server
#
# ensure .env exists
#
if [ ! -f ".env" ]; then
  cp .env.example .env
fi
#
#
#npm install --save-dev laravel-echo pusher-js
#
# set .env variables
#
sed -i 's/PUSHER_APP_KEY=$/PUSHER_APP_KEY=app-key/' .env
sed -i 's/PUSHER_APP_SECRET=$/PUSHER_APP_SECRET=app-secret/' .env
sed -i 's/PUSHER_APP_ID=$/PUSHER_APP_ID=app-id/' .env
#
echo '' >> .env
echo 'PUSHER_SCHEME=http' >> .env
echo 'PUSHER_HOST=localhost' >> .env
echo 'PUSHER_PORT=6001' >> .env
#
# add VITE missing variables in .env
#
echo '' >> .env
echo 'VITE_PUSHER_HOST="${PUSHER_HOST}"' >> .env
echo 'VITE_PUSHER_PORT="${PUSHER_PORT}"' >> .env
echo 'VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"' >> .env
